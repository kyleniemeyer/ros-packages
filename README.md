[![Toposens](https://toposens.com/wp-content/themes/toposens/assets/img/logo2.png)](https://toposens.com)


ROS packages for working with [Toposens 3D ultrasonic sensors](http://toposens.com/products/). Developed and tested for [ROS Kinetic](http://wiki.ros.org/kinetic) on [Ubuntu 16.04 (Xenial)](http://releases.ubuntu.com/16.04/) and [ROS Melodic](http://wiki.ros.org/melodic) on [Ubuntu 18.04 (Bionic)](http://releases.ubuntu.com/18.04/).

Detailed documentation for the packages along with a set of tutorials on using them with Toposens sonars can be found on our the ROS Wiki page: http://wiki.ros.org/toposens


# Setup ROS

This section is for users who are new to ROS (Robot Operating System) and need guidance in setting up their initial environment.

* Install ROS on your machine:

  * For Ubuntu 16.04 (Xenial): http://wiki.ros.org/kinetic/Installation/Ubuntu
  * For Ubuntu 18.04 (Bionic): http://wiki.ros.org/melodic/Installation/Ubuntu
---

* Install catkin tools:

  ```
  sudo apt install python-catkin-tools
  ```

* Setup your catkin workspace:

  ```
  mkdir -p ~/catkin_ws/src && cd ~/catkin_ws && catkin init
  ```

Our tutorials assume that you use *catkin tools* instead of the older *catkin_make* commands. If you are more comfortable with the latter, simply switch all occurrences of
`catkin build` in the documentation with `catkin_make_isolated`. If you are ready to switch an existing workspace that was previously built with `catkin_make`, call
`catkin clean` to enable builds with catkin tools.


# Tutorials

* [Getting Started with the TS3](http://wiki.ros.org/toposens/Tutorials/Getting%20Started%20with%20the%20TS3)

* [Using Multiple TS3 Sensors](http://wiki.ros.org/toposens/Tutorials/Using%20Multiple%20TS3%20Sensors)

* [Use a TS3 Sensor for Mapping with a TurtleBot3](http://wiki.ros.org/toposens/Tutorials/Use%20a%20TS3%20Sensor%20for%20Mapping%20with%20a%20TurtleBot3)

* [Getting Started with the TS3 on a Raspberry Pi](http://wiki.ros.org/toposens/Tutorials/Getting%20Started%20with%20the%20TS3%20on%20a%20Raspberry%20Pi)


# Code API

Doxygen results for all packages are stored on the ROS Docs server:

* [toposens_msgs](http://docs.ros.org/melodic/api/toposens_msgs/html/index-msg.html)
* [toposens_driver](http://docs.ros.org/melodic/api/toposens_driver/html/toposens_driver/annotated.html)
* [toposens_markers](http://docs.ros.org/melodic/api/toposens_markers/html/toposens_markers/annotated.html)
* [toposens_pointcloud](http://docs.ros.org/melodic/api/toposens_pointcloud/html/toposens_pointcloud/files.html)
* [toposens_sync](http://docs.ros.org/melodic/api/toposens_sync/html/annotated.html)


# Get Involved

Contribute to this open source project by submitting feature requests, reporting bugs and extending the packages with your own ideas. Open up a topic on our [issue tracker](https://gitlab.com/toposens/public/ros-packages/issues) to start a discussion with the admins.

Note that this repository conforms to best practices adopted by the ROS community. Please check your contributions against these community guidelines to ensure uniformity across the codebase:

* [Use Patterns and Best Practices](http://wiki.ros.org/ROS/Patterns)
* [Developer's Guide](http://wiki.ros.org/DevelopersGuide)
* [Coding Style Guide](http://wiki.ros.org/StyleGuide)
