^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package toposens_msgs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.2.1 (2019-07-12)
------------------

1.2.0 (2019-07-05)
------------------

1.1.0 (2019-06-21)
------------------

1.0.0 (2019-05-14)
------------------

0.9.6 (2019-05-13)
------------------
* Add colored tags to tests for console debugging
* Contributors: Adi Singh

0.9.5 (2019-05-03)
------------------

0.9.3 (2019-04-26)
------------------

0.9.2 (2019-04-17)
------------------
* Fixed package versions
* Contributors: Adi Singh

0.9.0 (2019-04-09)
------------------

0.8.1 (2019-03-29)
------------------
* Temperature calibration implemented
* Contributors: Sebastian Dengler

0.8.0 (2019-03-08)
------------------
* Created Command class with overloaded constructors
* Contributors: Christopher Lang

0.7.0 (2019-02-27)
------------------
* Added native PCL integration
* Implemented markers using rviz visual tools
* Added dynamic reconfigure capability for driver
* Refactored to standard ROS package template
* Contributors: Adi Singh

0.5.0 (2019-02-07)
------------------
