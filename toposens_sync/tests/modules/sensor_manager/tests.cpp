/** @file     sensor_manager_test.cpp
 *  @author   Sebastian Dengler
 *  @date     June 2019
 */

#include <fcntl.h>
#include <toposens_sync/sensor_manager.h>
#include <gtest/gtest.h>
#include <boost/thread.hpp>

using namespace toposens_sync;

class ManagerTest : public ::testing::Test
{
public:
  const std::string TAG = "\033[36m[SyncManagerTest]\033[00m - ";

protected:
  std::unique_ptr<SensorManager> sm;
  std::vector<toposens_msgs::TsScan> scans;
  ros::Subscriber sub;
  XmlRpc::XmlRpcValue mock_sensors;
  XmlRpc::XmlRpcValue driver_ports;
  std::string scan_frame;
  ros::NodeHandle* private_nh;
  ros::NodeHandle nh;
  boost::thread* sensorThreads[10];

  void SetUp()
  {
    private_nh = new ros::NodeHandle("~");
    private_nh->getParam("mock_sensors", mock_sensors);
    private_nh->getParam("ports", driver_ports);
    private_nh->getParam("scan_frame", scan_frame);
    sub = nh.subscribe(toposens_driver::kScansTopic, toposens_driver::kQueueSize, &ManagerTest::store, this);

    for (int i = 0; i < mock_sensors.size(); i++)
    {
      sensorThreads[i] = new boost::thread(sendReturnMessages, mock_sensors[i], scan_frame);
    }

  }

  void TearDown()
  {
    for (int i = 0; i < mock_sensors.size(); i++)
    {
      sensorThreads[i]->interrupt();
    }
    for (int i = 0; i < mock_sensors.size(); i++)
    {
      sensorThreads[i]->join();
    }

    delete private_nh;
    scans.clear();

  }

  void store(const toposens_msgs::TsScan::ConstPtr& msg)
  {
    scans.push_back(*msg);
    std::cerr << TAG << "\033[33m" << "\tReceived scan of size "
      << scans[scans.size()-1].points.size() << "\033[00m\n";
  }


  /** Simulate behavior of sensor:
    * - send acknowledgement when parameter command is received
    * - send scan frame when singlescan command received
    */
  static void sendReturnMessages(std::string port, std::string tx_data)
  {
    const int fd = open(port.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    tcflush(fd, TCIFLUSH);
    char rx_buff[100];
    std::string dummy_acknowledgement("S000000C00000E");

    while(true)
    {
      try
      {
        memset(&rx_buff, '\0', sizeof(rx_buff));
        boost::this_thread::sleep(boost::posix_time::milliseconds(10));

        if(read(fd, &rx_buff, sizeof(rx_buff)) > 0){
          // Send dummy acknowledgement for every reception
          write(fd,
                dummy_acknowledgement.c_str(),
                dummy_acknowledgement.size()
          );

          if (!strcmp(rx_buff, "CsMode00001\r"))
          {
            write(fd, tx_data.c_str(), tx_data.length());
          }
        }
      }
      catch (boost::thread_interrupted&)
      {
        close(fd);
        return;
      }
    }
  }

  // Test if received scan can be mapped to ASCII string of sent frame
  void EXPECT_FRAME(toposens_msgs::TsScan rcvd, std::string expct)
  {
    char buffer [100];
    sprintf(buffer, "S000000P0000X%05dY%05dZ%05dV%05dE", (int)(rcvd.points[0].location.x*1000),
                                                         (int)(rcvd.points[0].location.y*1000),
                                                         (int)(rcvd.points[0].location.z*1000),
                                                         (int)(rcvd.points[0].intensity*100));
    EXPECT_STREQ(buffer, expct.c_str());
  }

};


/**
  * Testing initialization of multiple sensors by a sensor manager
  */
TEST_F(ManagerTest, initializeSensors)
{
  std::cerr << TAG << "<initializeSensors>\n";
  std::cerr << TAG << "\tInitializing sensors...";

  EXPECT_NO_THROW(sm = std::make_unique<SensorManager>(nh, *private_nh));

  std::cerr << "done\n";
  std::cerr << TAG << "</initializeSensors>\n";
}

/**
 * Testing how SensorManager handles incomplete handover parameter.
 */
TEST_F(ManagerTest, corruptedParams)
{
  std::cerr << TAG << "<corruptedParams>\n";

  ros::NodeHandle* corrupted_params_nh;

  std::cerr << TAG << "\tInitialize with more frame ids than driver ports...";
    corrupted_params_nh = new ros::NodeHandle("~SyncManagerTestParams_missing_port");
    EXPECT_THROW(std::make_unique<SensorManager>(nh, *corrupted_params_nh), std::invalid_argument);
    delete corrupted_params_nh;
  std::cerr << "done\n";

  std::cerr << TAG << "\tInitialize with more than 10 frame_id params...";
    corrupted_params_nh = new ros::NodeHandle("~SyncManagerTestParams_more_than_10");
    EXPECT_THROW(std::make_unique<SensorManager>(nh, *corrupted_params_nh), std::invalid_argument);
    delete corrupted_params_nh;
  std::cerr << "done\n";

  std::cerr << TAG << "</corruptedParams>\n";
}

/**
  * Testing the functionality of a sensor manager instance to manage multiple
  * sensors, i.e. sending the scanning commands and polling the received frames
  */
TEST_F(ManagerTest, pollFrames)
{
  std::cerr << TAG << "<pollFrames>\n";

  sm = std::make_unique<SensorManager>(nh, *private_nh);

  sm->trigger();
  ros::Duration(0.01).sleep();
  ros::spinOnce();

  ASSERT_EQ(scans.size(), (uint)2) << "Not enough points received: " << scans.size();
  EXPECT_FRAME(scans[0], scan_frame);
  EXPECT_FRAME(scans[1], scan_frame);

  std::cerr << TAG << "</pollFrames>\n";
}


/**
  * Testing SensorManager::shutdown.
  * Desired output: Shutting down the sensor manager should shut down the sensor
  * instances that are managed by it. This should enable a new sensor manager
  * instance to instantiate new sensors on the same ports.
  */
TEST_F(ManagerTest, smShutdown)
{
  std::cerr << TAG << "<smShutdown>\n";

  sm = std::make_unique<SensorManager>(nh, *private_nh);

  std::cerr << TAG << "\tShutting down sensors...";

  ros::NodeHandle nh;
  sm->shutdown();
  std::cerr << "done\n";

  EXPECT_NO_THROW(std::make_unique<SensorManager>(nh, *private_nh));

  std::cerr << TAG << "</smShutdown>\n";
}


int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "ts_sync_sensor_manager_test");
  ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}
