/** @file     mapping/tests.cpp
 *  @author   Roua Mokchah
 *  @date     April 2019
 */
#include <ros/ros.h>
#include <gtest/gtest.h>
#include <ros/package.h>
#include <toposens_driver/sensor.h>
#include <toposens_pointcloud/mapping.h>


using namespace toposens_pointcloud;

class MappingTest : public ::testing::Test
{
public:
  const std::string TAG = "\033[36m[PointcloudMappingTest]\033[00m - ";

protected:
  // Defined in static tf broadcast in launch file.
  const double tf_x = 0.05;
  const double tf_y = -0.1;
  const double tf_z = 0.0;

  const int kNumPoints = 10;
  ros::NodeHandle* priv_nh;
  Mapping* m;

  TsCloud cloud;

  ros::Subscriber cloud_sub;
  ros::Publisher scans_pub;
  toposens_msgs::TsScan scan;
  std::vector<toposens_msgs::TsPoint> rcvd_points;

  void SetUp()
  {
    ros::NodeHandle nh;
    priv_nh = new ros::NodeHandle("~");
    m = new Mapping(nh, *priv_nh);

    scans_pub = nh.advertise<toposens_msgs::TsScan>(
      toposens_driver::kScansTopic, toposens_driver::kQueueSize);

    cloud_sub = nh.subscribe(kPointCloudTopic, 100, &MappingTest::store, this);

    scan.header.stamp = ros::Time(0);
    scan.header.frame_id = "toposens";

    ros::Duration(0.5).sleep();
  }

  void TearDown()
  {
    rcvd_points.clear();
    scan.points.clear();
    delete m;
    delete priv_nh;
  }

  /** Callback function for TsCloud topic */
  void store(const TsCloud::Ptr& msg)
  {
    for (auto p : msg->points) rcvd_points.push_back(p);
    std::cerr << TAG << "\033[33m" << "\tReceived "
      << rcvd_points.size() << " PCL points\n" << "\033[00m";
  }

  /** Listens for PCL points on #kPointCloudTopic. */
  void listen()
  {
    std::cerr << TAG << "\tListening for PCL points on " << kPointCloudTopic << "...\n";
    ros::Time end = ros::Time::now() + ros::Duration(1.0);
    while(ros::Time::now() < end)
    {
      ros::spinOnce();
      ros::Duration(0.1).sleep();
    }
  }

  /** Reads PCL points from file into the TsPoint variable. */
  void read()
  {
    std::string store_path = std::string(get_current_dir_name()) + "/toposens.pcd";
    std::cerr << TAG << "\tReading PCD data from " << store_path << "...";
    pcl::io::loadPCDFile<toposens_msgs::TsPoint>(
      store_path, cloud);
    std::cerr << "done\n";
  }

};

/** @test Expects zero PointCloud messages to be generated if an empty scan was published. */
TEST_F(MappingTest, emptyScan)
{
  std::cerr << TAG << "<emptyScan>\n";
  std::cerr << TAG << "\tPublishing empty scan...";

  scans_pub.publish(scan);
  std::cerr << "done\n";

  this->listen();
  EXPECT_EQ(rcvd_points.size(), 0);
  std::cerr << TAG << "</emptyScan>\n";
}

/** @test Expects no generated messages if a zero intensity scan was published.*/
TEST_F(MappingTest, zeroIntensityScan)
{
  std::cerr << TAG << "<zeroIntensityScan>\n";
  std::cerr << TAG << "\tPublishing scan with zero-intensity points...";

  for(int i = 0; i < kNumPoints; i++)
  {
    toposens_msgs::TsPoint pt;
    pt.location.x = pt.location.y = pt.location.z = (i + 1)/100.0;
    pt.intensity = 0;
    scan.points.push_back(pt);
  }

  scans_pub.publish(scan);
  std::cerr << "done\n";

  this->listen();
  EXPECT_EQ(rcvd_points.size(), 0);
  std::cerr << TAG << "</zeroIntensityScan>\n";
}

/** @test Tests that each incoming scan is converted to a new PointCloud message of
 *  template XYZI.
 */
TEST_F(MappingTest, validPoints)
{
  std::cerr << TAG << "<validPoints>\n";
  std::cerr << TAG << "\tPublishing scan with plottable points...";

  for(int i = 0; i < kNumPoints; i++)
  {
    toposens_msgs::TsPoint pt;
    pt.location.x = pt.location.y = pt.location.z = (i + 1)/100.0;
    pt.intensity = i + 1;
    scan.points.push_back(pt);
  }

  scans_pub.publish(scan);
  std::cerr << "done\n";

  this->listen();
  ASSERT_EQ(rcvd_points.size(), kNumPoints) << "Not enough points in scan: " << scan.points.size();

  for (int i = 0; i < kNumPoints; i++)
  {
    auto expc_pt = scan.points.at(i);
    auto rcvd_pt = rcvd_points.at(i);

    // @todo confirm if EXPECT_NEAR is working
    EXPECT_NEAR(expc_pt.location.x + tf_x, rcvd_pt.location.x, 1.0e-6);
    EXPECT_NEAR(expc_pt.location.y + tf_y, rcvd_pt.location.y, 1.0e-6);
    EXPECT_NEAR(expc_pt.location.z + tf_z, rcvd_pt.location.z, 1.0e-6);
    EXPECT_FLOAT_EQ(expc_pt.intensity, rcvd_pt.intensity);
  }
  std::cerr << TAG << "</validPoints>\n";
}

/** @test Generates half of the points with 0 intensity, half with non-zero intensity and publishes them.
 *  Listens to the generated scan and attempts to save the points to a file.
 *  Checks if the resulted point cloud width (half of the points with non-zero intensity) is equal to #kNumPoints/2
 */
TEST_F(MappingTest, saveData)
{
  std::cerr << TAG << "<saveData>\n";
  std::cerr << TAG << "\tPublishing scan with mixed points...";

  for(int i = 0; i < kNumPoints; i++)
  {
    toposens_msgs::TsPoint pt;
    pt.location.x = pt.location.y = pt.location.z = (i + 1)/100.0;
    pt.intensity = (i % 2) ? i : 0; // Alternate intensity between 0 and i.
    scan.points.push_back(pt);
  }

  scans_pub.publish(scan);
  std::cerr << "done\n";

  this->listen();
  std::cerr << TAG << "\tMapping::save on custom path...";
  m->save();
  std::cerr << "done\n";

  this->read();
  EXPECT_EQ(cloud.width, kNumPoints/2);

  std::cerr << TAG << "</saveData>\n";
}

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "ts_pointcloud_mapping_test");
  ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}
