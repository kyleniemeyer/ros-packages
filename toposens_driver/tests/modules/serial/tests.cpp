/** @file     serial_test.cpp
 *  @author   Christopher Lang, Roua Mokchah, Adi Singh
 *  @date     March 2019
 */

#include <fcntl.h>
#include <ros/ros.h>
#include <gtest/gtest.h>

#include <toposens_driver/serial.h>

using namespace toposens_driver;

/** This class uses two connected mock ports for sending and receivig data virtually.*/
class SerialTest : public ::testing::Test
{
public:
  const std::string TAG = "\033[36m[DriverSerialTest]\033[00m - ";

protected:
  std::string mock_sensor, driver_port;
  ros::NodeHandle* private_nh;

  void SetUp()
  {
    private_nh = new ros::NodeHandle("~");
    private_nh->getParam("mock_sensor", mock_sensor);
    private_nh->getParam("port", driver_port);
  }

  void TearDown()
  {
    delete private_nh;
  }

};

/** @test Attempts to open a serial connection with invalid port values.
 *  Should throw an exception in each case.
 */
TEST_F(SerialTest, openInvalidPort)
{
  std::cerr << TAG << "<openInvalidPort>\n";

  std::cerr << TAG << "\tAttempting connection to null port...";
  EXPECT_THROW(Serial(""), std::runtime_error);
  std::cerr << "done\n";

  std::cerr << TAG << "\tAttempting connection to non-existent port...";
  EXPECT_THROW(Serial("tty42"), std::runtime_error);
  std::cerr << "done\n";

  std::cerr << TAG << "</openInvalidPort>\n";
}


/** @test Attempts to open a serial connection with valid port values.
 *  Expects no exception to be thrown. 
 */
TEST_F(SerialTest, openValidPort)
{
  std::cerr << TAG << "<openValidPort>\n";

  std::cerr << TAG << "\tAttempting connection to virtual sensor port...";
  EXPECT_NO_THROW(Serial(mock_sensor.c_str()));
  std::cerr << "done\n";

  std::cerr << TAG << "\tAttempting connection to virtual driver port...";
  EXPECT_NO_THROW(Serial(driver_port.c_str()));
  std::cerr << "done\n";

  std::cerr << TAG << "</openValidPort>\n";
}

/** @test Checks that a valid frame transmitted over an emulated port is received
 *  correctly at the other end via Serial::getFrame.
 */
TEST_F(SerialTest, getValidFrame)
{
  std::cerr << TAG << "<getValidFrame>\n";

  const char tx_data[] = "S000016P0000X-0415Y00010Z00257V00061E";
  const int conn_fd = open(mock_sensor.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);

  std::cerr << TAG << "\tTx data: " << tx_data << "\n";
  std::cerr << TAG << "\tManually transmitting over " << mock_sensor << "...";
  write(conn_fd, tx_data, sizeof(tx_data));
  std::cerr << "done\n";

  std::cerr << TAG << "\tSerial::getFrame over " << driver_port << "...";
  Serial serial(driver_port);
  std::stringstream ss;
  serial.getFrame(ss);
  std::cerr << "done\n";

  std::string rx_data = ss.str();
  std::cerr << TAG << "\tRx data: " << rx_data << "\n";

  EXPECT_STREQ(rx_data.c_str(),tx_data);
  close(conn_fd);

  std::cerr << TAG << "</getValidFrame>\n";
}

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "ts_driver_serial_test");
  ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}
